package controller;

import model.Dwelling;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DwellingController {

    private static DwellingController instance = null;
    ArrayList<Dwelling> dwellings = new ArrayList<Dwelling>();

    public static DwellingController getInstance() {
        if (instance == null) {
            instance = new DwellingController();
        }
        return instance;
    }

    public boolean createDwellings(String JSONArray) {
        try {
            // from the json array with dwelling objects we have to create dwelling object and add it to the dwelling array
            JSONObject jObject = new JSONObject(JSONArray);
            org.json.JSONArray jArray = jObject.getJSONArray("dwellings");

            // loop through the jsonarray
            for (int i = 0; i < jArray.length(); i++) {
                JSONObject jsonDwelling = jArray.getJSONObject(i);
                int id = jsonDwelling.getInt("id");
                int VHE = jsonDwelling.getInt("VHE");
                String streetName = jsonDwelling.getString("streetName");
                int houseNumber = jsonDwelling.getInt("houseNumber");
                String city = jsonDwelling.getString("city");
                String country = jsonDwelling.getString("country");
                double price = jsonDwelling.getDouble("price");

                Dwelling dwelling = new Dwelling(id, VHE, streetName, houseNumber, city, country, price);
                dwellings.add(dwelling);
            }

        } catch (JSONException e) {
            // if there went something wrong while creating the dwelling objects we return false
            return false;
        }
        // return true when all dwellings are created
        return true;
    }

    public ArrayList<Dwelling> getDwellings() {
        // return the arraylist with dwellings
        return this.dwellings;
    }

    public Dwelling getDwellingById(int dwellingId) {
        // loop through the array list with dwellings and try to find a dwelling with the given id. When no dwelling
        // is found return null
        for(Dwelling dwelling : dwellings) {
            if(dwelling.getId() == dwellingId) {
                return dwelling;
            }
        }
        return null;
    }

    public boolean deleteDwellingById(int dwellingId) {
        // delete dwelling from the dwelling array with the given id
        for(Dwelling dwelling : dwellings) {
            if(dwelling.getId() == dwellingId) {
                dwellings.remove(dwelling);
                return true;
            }
        }
        return false;
    }
}
