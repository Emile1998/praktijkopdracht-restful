package services;

import controller.DwellingController;
import model.Dwelling;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;
import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/dwellingservice")
public class DwellingService {

    @POST
    @Path("/dwellings/create")
    @Consumes("application/json")
    public Response createDwelling(String JSONArray) {
        // ask the dwelling controller to create a new dwelling with the given information
        boolean result = DwellingController.getInstance().createDwellings(JSONArray);
        // if the dwellingcontroller returns true the dwelling is added succesfully, else we have to let the user
        // know that something went wrong
        if(result) {
            return Response.status(200).entity("Dwelling created succesfully!").build();
        } else {
            return Response.status(500).entity("Something went wrong while creating this dwelling, please try again!").build();
        }
    }

    @GET
    @Path("/dwellings")
    public Response getAllDwellings() {
        // Ask the dwelling controller to return all created dwellings, if it returns an empty array there are currently
        // no dwellings available
        JSONArray jsonArray = new JSONArray(DwellingController.getInstance().getDwellings());
        return Response.status(200).entity(jsonArray.length() == 0 ? "We didn't find any dwellings" : jsonArray.toString()).build();
    }

    @GET
    @Path("/dwellings/{dwellingid}")
    public Response getDwellingById(@PathParam("dwellingid") int dwellingid) {
        // Ask the dwelling controller to get a dwelling with a given idea
        Dwelling dwelling = DwellingController.getInstance().getDwellingById(dwellingid);
        // if the dwelling object is null there were no dwellings found, return a error message to the user
        if(dwelling != null) {
            // we have to create a valid json object from the dwelling
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("id", dwelling.getId());
                jsonObject.put("VHE", dwelling.getVHE());
                jsonObject.put("streetName", dwelling.getStreetName());
                jsonObject.put("houseNumber", dwelling.getHouseNumber());
                jsonObject.put("city", dwelling.getCity());
                jsonObject.put("country", dwelling.getCountry());
                jsonObject.put("price", dwelling.getPrice());
            } catch (JSONException e) {
                // if something went wrong while creating the json object we have to return an internal server error
                return Response.status(500).entity("Oops, something went wrong!").build();
            }
            // everything went right, return a 200 http status code with the created json object
            return Response.status(200).entity(jsonObject.toString()).build();
        }
        // the dwelling was not found, return an error message
        return Response.status(404).entity("There is no dwelling with the given id").build();
    }

    @GET
    @Path("/dwellings/delete/{dwellingid}")
    public Response deleteDwellingById(@PathParam("dwellingid") int dwellingid) {
        // ask the dwelling controller to delete the dwelling
        boolean result = DwellingController.getInstance().deleteDwellingById(dwellingid);
        // if the dwelling controller returns true the dwelling was deleted succesfully
        if (result) {
            return Response.status(200).entity("Dwelling succesfully deleted").build();
        } else {
            // if the dwelling controller returns false there was no dwelling with the given id, return an error message
            return Response.status(404).entity("There is no dwelling with the given id").build();
        }
    }
}
