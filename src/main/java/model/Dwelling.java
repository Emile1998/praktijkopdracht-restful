package model;

public class Dwelling {

    private int id;
    private int VHE;
    private String streetName;
    private int houseNumber;
    private String city;
    private String country;
    private double price;

    public Dwelling(int id, int VHE, String streetName, int houseNumber, String city, String country, double price) {
        this.id = id;
        this.VHE = VHE;
        this.streetName = streetName;
        this.houseNumber = houseNumber;
        this.city = city;
        this.country = country;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVHE() {
        return VHE;
    }

    public void setVHE(int VHE) {
        this.VHE = VHE;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public int getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(int houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Dwelling{" +
                "id=" + id +
                ", VHE=" + VHE +
                ", streetName='" + streetName + '\'' +
                ", houseNumber=" + houseNumber +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                ", price=" + price +
                '}';
    }
}