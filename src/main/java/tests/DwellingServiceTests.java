package tests;

import org.junit.Assert;
import org.junit.Test;
import services.DwellingService;

import javax.ws.rs.core.Response;

public class DwellingServiceTests {
    DwellingService dwellingService = new DwellingService();

    // Deze test is om te kijken wat er gebeurt als we een woning met id opvragen die niet bestaat
    @Test
    public void testGetDwellingById() {
        Response response= dwellingService.getDwellingById(100);
        String expected = "{\"Result\":\"There is no dwelling with the given id\"}";
        Assert.assertEquals(expected, response.getEntity().toString());
    }

    // Hier testen we of de woning goed wordt opgeslagen en of deze dan opgevraagt kan worden met zijn id
    @Test
    public void testCreateDwelling() {
        dwellingService.createDwelling(
                "{\"dwellings\":[{" +
                        "\"id\": \"12\"," +
                        "\"VHE\": \"123\"," +
                        "\"streetName\": \"Daltonlaan\"," +
                        "\"houseNumber\": \"200\"," +
                        "\"city\": \"Utrecht\"," +
                        "\"country\": \"Netherlands\"," +
                        "\"price\": \"10.00\"" +
                        "}]}");

        Response response = dwellingService.getDwellingById(12);
        String expected = "{\"Result\":\"{\\\"id\\\":12,\\\"VHE\\\":123,\\\"streetName\\\":\\\"Daltonlaan\\\"," +
                "\\\"houseNumber\\\":200,\\\"city\\\":\\\"Utrecht\\\",\\\"country\\\":\\\"Netherlands\\\"," +
                "\\\"price\\\":10}\"}";
        Assert.assertEquals(expected, response.getEntity().toString());
    }
}
